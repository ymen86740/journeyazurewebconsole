﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using System;

namespace JourneyAppServicesManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccessController : ControllerBase
    {
        [HttpGet("GetAccessToken")]
        [EnableCors("CorsPolicy")]
        public ActionResult<VerifyResponse> GetAccessToken()
        {
            try
            {
                var client = new RestClient("https://login.microsoftonline.com");
                var request = new RestRequest("ab8924e7-2fa2-499f-99ca-b08fa75b70bc/oauth2/token", Method.POST);
                request.AddParameter("grant_type", "client_credentials");
                request.AddParameter("client_id", "86569e79-cc92-47c7-8b8c-5d4ca6090e3d");
                request.AddParameter("client_secret", "_:dNq3T7a+AuSjc*te5rSChP1akH4Yr7");
                request.AddParameter("resource", "https://management.azure.com");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                IRestResponse<VerifyResponse> response = client.Execute<VerifyResponse>(request);
                return response.Data;
            }
            catch
            {
                return new VerifyResponse{
                    token_type="error"
                };
            }
        }
    }

    public class VerifyResponse
    {
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string ext_expires_in { get; set; }
        public string expires_on { get; set; }
        public string not_before { get; set; }
        public string resource { get; set; }
        public string access_token { get; set; }
    }
}