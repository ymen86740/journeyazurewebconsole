import React from 'react';
import {Route,Switch} from "react-router-dom";
import appServicesManager from './pages/appServicesManager';

export default ()=>(
    <div>
      <Switch>
        <Route exact path='/' component={appServicesManager}></Route>
      </Switch>
    </div>
)
