import React from 'react';
import { Row, Col  } from 'antd';
import AppServicesManagerPanel from '../components/AppServicesManagerPanel';
import logo from '../static/logo.png';

export default ()=>(
    <div>
        <Row>
             <div style={{height:'50px',boxShadow:'4px 0px 8px rgba(0,0,0,0.5)'}}>
                 <img src={logo} alt='' style={{height:'50px'}}></img>
            </div>
        </Row>
        <Row>
            <Col span={22} offset={1}>
                 <AppServicesManagerPanel></AppServicesManagerPanel>
            </Col>
        </Row>
    </div>
)