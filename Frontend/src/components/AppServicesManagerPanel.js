import React, { Component } from 'react';
import {Row,Col,Table,Divider,Tag,Button,message,Input,Breadcrumb,Icon,Popconfirm} from 'antd';
import axios from 'axios';

export default class ResubmitOrderPanel extends Component{
    constructor(props){
        super(props);
        this.state={
            accessToken:'',
            appServicesList:[],
            searchAppServicesList:[],
            searchLoading:true,
            filterName:undefined,
            tableColumns:[
                {
                    title:'App Name',
                    dataIndex:'name'
                },
                {
                    title:'Status',
                    dataIndex:'properties.state',
                    render: x=>(
                        x==='Running'?<Tag color="green">Running</Tag>:<Tag color="red">Stopped</Tag>
                    )
                },
                {
                    title:'Location',
                    dataIndex:'location',
                },
                {
                    title:'Last Modified Time',
                    dataIndex:'properties.lastModifiedTimeUtc',
                    render:x=>(
                        new Date(x).toLocaleString()
                    )
                },
                {
                    title:'ResourceGroup',
                    dataIndex:'properties.resourceGroup',
                },
                {
                    title:'',
                    align:'left',
                    render:x=>(
                        <Row type="flex" justify="space-between" style={{width:'210px'}}>
                            {
                                x.properties.state==='Running'?<Button type="primary" disabled={x.properties.state==='Running'?true:false} style={{backgroundColor:'#87d068',borderColor:'#87d068'}}>Start</Button>:
                                <Popconfirm placement="topLeft" title={"Start App Service: "+x.name} okText="Yes" cancelText="No" icon={<Icon type="question-circle-o"></Icon>} onConfirm={()=>{
                                    message.loading('Starting',100);
                                    this.setState({searchLoading:true});
                                    axios.post("https://management.azure.com/subscriptions/ac6bda66-0e84-41fe-847a-d6eb57d71bfb/resourceGroups/"+x.properties.resourceGroup+"/providers/Microsoft.Web/sites/"+x.name+"/start?api-version=2016-08-01",null,{headers:{'Authorization':'Bearer '+this.state.accessToken}}).then(res=>{
                                        if(res.status===200){
                                            this.refreshAppServicesState('start',x.name);
                                        }
                                        else{1
                                            message.destroy();
                                            message.error("Error Code: "+res.status,6);
                                            this.setState({searchLoading:false});
                                        }
                                    });
                                }}>
                                    <Button type="primary" disabled={x.properties.state==='Running'?true:false} style={{backgroundColor:'#87d068',borderColor:'#87d068'}}>Start</Button>
                                </Popconfirm>
                            }
                            {
                                x.properties.state!=='Running'?<Button type="primary" disabled={x.properties.state==='Running'?false:true} style={{backgroundColor:'red',borderColor:'red'}}>Stop</Button>:
                                <Popconfirm placement="top" title={'Stop App Service: '+x.name} okText="Yes" cancelText="No" icon={<Icon type="question-circle-o"></Icon>} onConfirm={()=>{
                                    message.loading('Stopping',100);
                                    this.setState({searchLoading:true});
                                    axios.post("https://management.azure.com/subscriptions/ac6bda66-0e84-41fe-847a-d6eb57d71bfb/resourceGroups/"+x.properties.resourceGroup+"/providers/Microsoft.Web/sites/"+x.name+"/stop?api-version=2016-08-01",null,{headers:{'Authorization':'Bearer '+this.state.accessToken}}).then(res=>{
                                        if(res.status===200){
                                            this.refreshAppServicesState('stop',x.name);
                                        }
                                        else{
                                            message.destroy();
                                            message.error("Error Code: "+res.status,6);
                                            this.setState({searchLoading:false});
                                        }
                                    });
                                }}>
                                   <Button type="primary" disabled={x.properties.state==='Running'?false:true} style={{backgroundColor:'red',borderColor:'red'}}>Stop</Button>
                               </Popconfirm>
                            }
                            <Popconfirm placement="topRight" title={'Restart App Service: '+x.name} okText="Yes" cancelText="No" icon={<Icon type="question-circle-o"></Icon>} onConfirm={()=>{
                                message.loading('Restarting',100);
                                this.setState({searchLoading:true});
                                axios.post("https://management.azure.com/subscriptions/ac6bda66-0e84-41fe-847a-d6eb57d71bfb/resourceGroups/"+x.properties.resourceGroup+"/providers/Microsoft.Web/sites/"+x.name+"/restart?api-version=2016-08-01",null,{headers:{'Authorization':'Bearer '+this.state.accessToken}}).then(res=>{
                                    if(res.status===200){
                                        this.refreshAppServicesState('restart',x.name);
                                    }
                                    else{
                                        message.destroy();
                                        message.error("Error Code: "+res.status,6);
                                        this.setState({searchLoading:false});
                                    }
                                });
                             }}>
                                <Button type="primary" style={{backgroundColor:'#1890ff',borderColor:'#1890ff'}}>Restart</Button>
                            </Popconfirm>
                        </Row>
                    )
                }
            ]
        };
    }

    render(){
        return(
            <div>
                <Row>
                    <div style={{height:'30px'}}></div>
                </Row>
                <Row>
                    <Breadcrumb>
                        <Breadcrumb.Item><Icon type="home"></Icon></Breadcrumb.Item>
                        <Breadcrumb.Item>App Services Management</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <Row>
                    <div style={{height:'30px'}}></div>
                </Row>
                <Row>
                    <Col span={4}>
                        <Input.Search placeholder="Filter by name" disabled={this.state.searchLoading} onKeyUp={this.handleSearch}></Input.Search>
                    </Col>
                </Row>
                <Row>
                <Divider />
                    <Table rowKey="id" columns={this.state.tableColumns} dataSource={this.state.searchAppServicesList} loading={this.state.searchLoading}></Table>
                </Row>
            </div>
        )
    }

    componentDidMount(){
        this.initData();
    }

    initData=()=>{
        this.setState({searchLoading:true});
        if(this.state.accessToken===''){
            axios.get('https://journeyapp.azurewebsites.net/api/Access/GetAccessToken/').then((res)=>{
                this.setState({accessToken:res.data.access_token},()=>{
                    axios.get('https://management.azure.com/subscriptions/ac6bda66-0e84-41fe-847a-d6eb57d71bfb/providers/Microsoft.Web/sites?api-version=2016-08-01',{headers:{'Authorization':'Bearer '+this.state.accessToken}}).then((res)=>{  
                        this.setState({appServicesList:res.data.value,
                                       searchAppServicesList:res.data.value,
                        },x=>{this.setState({searchLoading:false})});
                    });
                });
            });
        }
        else{
            axios.get('https://management.azure.com/subscriptions/ac6bda66-0e84-41fe-847a-d6eb57d71bfb/providers/Microsoft.Web/sites?api-version=2016-08-01',{headers:{'Authorization':'Bearer '+this.state.accessToken}}).then((res)=>{  
                this.setState({appServicesList:res.data.value,
                               searchAppServicesList:res.data.value
                },x=>{this.setState({searchLoading:false})});
            });
        }
    }

    handleSearch=(e)=>{
        this.setState({fileterName:e.target.value});
        if(e.target.value===undefined){
            this.setState({searchAppServicesList:this.state.appServicesList});
        }
        else{
            this.setState({searchAppServicesList:this.state.appServicesList.filter(x=>{
                return x.name.match(new RegExp('^'+e.target.value,"gi"));
            })});
        }
    }

    refreshAppServicesState=(action,appName)=>{
        axios.get('https://management.azure.com/subscriptions/ac6bda66-0e84-41fe-847a-d6eb57d71bfb/providers/Microsoft.Web/sites?api-version=2016-08-01',{headers:{'Authorization':'Bearer '+this.state.accessToken}}).then((res)=>{  
            this.setState({appServicesList:res.data.value,
                           searchAppServicesList:res.data.value.filter(x=>{
                                return x.name.match(new RegExp('^'+this.state.fileterName,"gi")
                           );
                })
            },x=>{this.setState({searchLoading:false},()=>{
                message.destroy();
                switch(action){
                    case 'start':message.success("App Service "+appName+" has been Successfully Started",6);break;
                    case 'stop':message.success("App Service "+appName+" has been Successfully Stopped",6);break;
                    case 'restart':message.success("App Service "+appName+" has been Successfully Restarted",6);break;
                    default:
                }
            })});
        });
    }
}